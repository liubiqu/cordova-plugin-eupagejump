//
//  EUPageJumpViewController.m
//
//  Created by WuJianNan on 11/21/14.
//  Copyright (c) 2014 nd.com.cn. All rights reserved.
//

#import <Cordova/CDVViewController.h>
#import <Cordova/CDVCommandDelegateImpl.h>
#import <Cordova/CDVCommandQueue.h>
@class EUPageJumpViewController;

@protocol EUPageJumpViewControllerDebugDelegate <NSObject>
- (void)viewDidLoad:(EUPageJumpViewController *)pageJumpViewController;
- (BOOL)shouldInjectWeinreJS:(EUPageJumpViewController *)pageJumpViewController;
- (BOOL)shouldShowWebViewProgressView:(EUPageJumpViewController *)pageJumpViewController;
- (NSString *)weireJSURL:(EUPageJumpViewController *)pageJumpViewController;
- (BOOL)shoudlInjectDebugGapJS:(EUPageJumpViewController *)pageJumpViewController;
- (void)didPageJumpViewControllerWillAppear:(EUPageJumpViewController *)pageJumpViewController;
@end

@interface EUPageJumpViewController : CDVViewController
// fromData和resumeData在被EUPageJump取过一次之后，将会被设置为空
@property(nonatomic, strong) NSDictionary *fromData;
@property(nonatomic, strong) NSDictionary *resumeData;
@property(nonatomic, strong) NSDictionary *jumpControlData;

/**
 *  @Author WuJianNan
 *
 *  当一个page被退出的时候，其上一个page将会被调用这个方法，用于触发
 *  PageJump插件对应push和present的onClose方法
 */
- (void)onNextPageClose;

/**
 *  @Author WuJianNan
 *
 *  初始化方法，由FromPage的EUPageJump实例调用
 *
 *  @param fromData       从FromPage传递过来的参数
 *  @param jumpControlDic 跳转动作的控制参数
 *
 *  @return 初始化过的 vc 实例
 */
- (id)initWithFromData:(NSDictionary *)fromData jumpControlDate:(NSDictionary *)jumpControlData;

/**
 *  @Author WuJianNan
 *
 *  获取该vc的pageID
 *
 *  @return pageID
 */
- (NSString *)getCurrentPageID;
/**
 *  @author liubiqu@qq.com, 15-08-18 17:08:43
 *
 *  刷新界面
 *
 *  @return 刷新界面
 */
- (void)reloadWebView;

/**
 *  @Author WuJianNan
 *
 *  注册debugDelegate
 *
 *  @param delegate
 */
+ (void)regisiterDebugDelegate:(id)delegate;
@end

/**
 *  @Author WuJianNan
 *
 *  预留自定义CommandDelegate、CommandQueue。用于以后可能需要的扩展。
 *  目前EUBasicViewController使用的是CDVCommandDelegateImpl、CDVCommandQueue
 */
@interface EUPageJumpCommandDelegate : CDVCommandDelegateImpl
@end

@interface EUPageJumpCommandQueue : CDVCommandQueue
@end
