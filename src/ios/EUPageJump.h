//
//  EUPageJump.h
//
//  Created by WuJianNan on 11/20/14.
//  Copyright (c) 2014 nd.com.cn. All rights reserved.
//

#import <Cordova/CDVPlugin.h>
#import "EUPageJumpConstants.h"
//----------------------------------XcodeColors开始--------------------------------------
#define XCODE_COLORS_ESCAPE_MAC @"\033["
#define XCODE_COLORS_ESCAPE_IOS @"\xC2\xA0["

#if 0
#define XCODE_COLORS_ESCAPE XCODE_COLORS_ESCAPE_IOS
#else
#define XCODE_COLORS_ESCAPE XCODE_COLORS_ESCAPE_MAC
#endif

#define XCODE_COLORS_RESET_FG XCODE_COLORS_ESCAPE @"fg;" // Clear any foreground color
#define XCODE_COLORS_RESET_BG XCODE_COLORS_ESCAPE @"bg;" // Clear any background color
#define XCODE_COLORS_RESET XCODE_COLORS_ESCAPE @";"      // Clear any foreground or background color

//#define LogBlue(frmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg0,0,255;" frmt XCODE_COLORS_RESET), ##__VA_ARGS__)

#define NSLogInfo(frmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg0,255,0;" frmt XCODE_COLORS_RESET), ##__VA_ARGS__)
#define NSLogWarn(frmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,0,255;" XCODE_COLORS_ESCAPE @"bg204,204,204;" frmt XCODE_COLORS_RESET), ##__VA_ARGS__)
#define NSLogError(frmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,0,0;" frmt XCODE_COLORS_RESET), ##__VA_ARGS__)
#define NSLogDebug(frmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg153,102,51;" XCODE_COLORS_ESCAPE @"bg204,204,204;" frmt XCODE_COLORS_RESET), ##__VA_ARGS__)
//----------------------------------XcodeColors结束--------------------------------------

@interface EUPageJump : CDVPlugin

//所有传入的command，arguments[0]为跳转时传递的数据字典。arguments[1] 为跳转的控制参数字典，可能为空
+ (UIViewController *)getInstanceVC:(NSString *)controllerName;
#pragma mark - nav jump methods

- (void)pushPage:(CDVInvokedUrlCommand *)command;
- (void)popPage:(CDVInvokedUrlCommand *)command;
/**
 *  @author liubiqu@qq.com, 15-08-18 17:08:32
 *
 *  模拟window.history.back()方法，popPage并刷新上层界面
 *
 *  @param command <#command description#>
 */
- (void)popToRoot:(CDVInvokedUrlCommand *)command;
- (void)popToPageWithPageID:(CDVInvokedUrlCommand *)command;

#pragma mark - mode jump methods

- (void)presentModelPage:(CDVInvokedUrlCommand *)command;
- (void)dismissModelPage:(CDVInvokedUrlCommand *)command;

#pragma mark - refresh methods

- (void)refreshPage:(CDVInvokedUrlCommand *)command;

#pragma mark - util methods

- (void)onNextPageClose;

//页面在onDeviceReady之后，首先需要调用这个方法来获取由其他页面传递过来的初始化数据，可能为空
- (void)getFromData:(CDVInvokedUrlCommand *)command;

//当页面再次将要被显示的时候，调用该方法来获取由其他页面回传的数据，可能为空
- (void)getResumeData:(CDVInvokedUrlCommand *)command;

//获取该页面的pageID（pageID为原生页面的唯一标识符，也可以当做webView实例的唯一标识符）
- (void)getCurrentPageID:(CDVInvokedUrlCommand *)command;

//获取当前页面在页面堆栈中的序号（model模式下，pageIndex总是为零）
- (void)getCurrentPageIndex:(CDVInvokedUrlCommand *)command;

@end
