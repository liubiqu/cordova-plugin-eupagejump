

#import <Foundation/Foundation.h>

#undef njk_weak
#if __has_feature(objc_arc_weak)
#define njk_weak weak
#else
#define njk_weak unsafe_unretained
#endif

extern const float EUPageJumpInitialProgressValue;
extern const float EUPageJumpInteractiveProgressValue;
extern const float EUPageJumpFinalProgressValue;

typedef void (^EUPageJumpWebViewProgressBlock)(float progress);
@protocol EUPageJumpWebViewProgressDelegate;
@interface EUPageJumpWebViewProgress : NSObject <UIWebViewDelegate>
@property(nonatomic, njk_weak) id<EUPageJumpWebViewProgressDelegate> progressDelegate;
@property(nonatomic, njk_weak) id<UIWebViewDelegate> webViewProxyDelegate;
@property(nonatomic, copy) EUPageJumpWebViewProgressBlock progressBlock;
@property(nonatomic, readonly) float progress; // 0.0..1.0

- (void)reset;
@end

@protocol EUPageJumpWebViewProgressDelegate <NSObject>
- (void)webViewProgress:(EUPageJumpWebViewProgress *)webViewProgress updateProgress:(float)progress;
@end
