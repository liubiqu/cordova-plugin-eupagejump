//
//  EUPageJumpConstants.h
//
//  Created by WuJianNan on 11/20/14.
//  Copyright (c) 2014 nd.com.cn. All rights reserved.
//

/*
 * Note: Make sure these defines match PageJumpConstants.js
 */

#ifndef CustomPlugin_EUPageJumpConstants_h
#define CustomPlugin_EUPageJumpConstants_h

#define kControlTitleKey @"title"

#define kControlControllerName @"controllerName"
#define kControlActivityName @"activityName"

#define kControlTargetPagePathKey @"targetPagePath"
#define kControlTargetModuleIdKey @"targetModuleId"

#define kControlTransitionWithAnimKey @"transitionWithAnim"
#define kControlTransitionModelAnimTypeKey @"transitionModelAnimType"

#define kControlFromPageIDKey @"fromPageID"
#define kControlTargetPageIDKey @"targetPageID"
#define kControlTransitionTypeKey @"transitionType"

typedef enum : NSUInteger { WIDTHOUT = 0, WIDTH = 1 } ControlTransitionWithAnimType;

typedef enum : NSUInteger { NAV = 0, MODEL = 1 } ControlTransitionType;

typedef enum : NSUInteger { CoverVertical = 0, FlipHorizontal = 1, CrossDissolve = 2, PartialCurl = 3 } ControlTransitionModelAnimType;

#endif
