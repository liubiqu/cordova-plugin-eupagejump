package com.nd.pagejump;

import android.content.Context;

import org.apache.cordova.CordovaWebView;

/**
 * Created by Ivan Aldrich on 2015/8/13.
 */
public interface PageJumpProto {

    public void jumpToPage(Context context, CordovaWebView cordovaWebView, String paramsString);

    public void pageBack(Context context, CordovaWebView cordovaWebView);
}
