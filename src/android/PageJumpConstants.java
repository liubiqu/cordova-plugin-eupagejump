package com.nd.pagejump;

public class PageJumpConstants {

	public static final String ControlFullPath = "ControlFullPath";
	public static final String ControlTitleKey = "title";
    public static final String ControlControllerName = "controllerName";//适于iOS的控制器名称
    public static final String ControlActivityName = "activityName"; //适于于安卓的Activity名称
	public static final String ControlTargetPagePathKey = "targetPagePath";
	
	public static final String ControlTransitionWithAnimKey = "transitionWithAnim";
	public static final String ControlTransitionModelAnimTypeKey = "transitionModelAnimType";
	
	public static final String ControlTargetPageIDKey = "targetPageID";
	public static final String ControlFromPageIDKey = "fromPageID";
	public static final String ControlTransitionTypeKey = "transitionType";
	
	
	public static final String Control_Action_SendData = "Control_Action_SendData";
	public static final String ACTION_RESULT = "ACTION_RESULT";

   
}
