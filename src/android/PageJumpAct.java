package com.nd.pagejump;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;

import org.apache.cordova.CordovaActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 跳转插件
 * 修改修改cordovalib里的
 * else if ("spinner".equals(id)) {
        if ("stop".equals(data.toString())) {
                this.spinnerStop();
                this.appView.setVisibility(View.VISIBLE);// 应为PageJumpAct要进行动画显示，所以将这句注释了
        }
        解决动画会闪动的问题
 * 
 * @author Administrator
 *
 */
public class PageJumpAct extends CordovaActivity {

	public static void launch(Context context,String h5DefaultPathString) {
		Intent intent = new Intent();
		intent.setClass(context, PageJumpAct.class);
		JSONArray array = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(PageJumpConstants.ControlTargetPagePathKey,h5DefaultPathString);
			
			array.put(new JSONObject());
			array.put(jsonObject);
			intent.putExtra(EUPageJump.PAGEJUMP_TARGET, array.toString());
			
			context.startActivity(intent);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	JSONObject jsonObject;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		preferences.set("backgroundColor", Color.TRANSPARENT);
		preferences.set("Fullscreen", false);
		Intent intent = getIntent();
		if (intent != null) {
			String jsonObjectString = intent.getStringExtra(EUPageJump.PAGEJUMP_TARGET);
			try {
				JSONArray jsonArray = new JSONArray(jsonObjectString);
				jsonObject = jsonArray.getJSONObject(0);
				JSONObject jsonObject = jsonArray.getJSONObject(1);
				String filePathString = jsonObject.get(PageJumpConstants.ControlTargetPagePathKey).toString();
				boolean isFullPath = false;
				try {
					isFullPath = jsonObject.getBoolean(PageJumpConstants.ControlFullPath);
				} catch (Exception e) {
                    e.printStackTrace();
				}

				if (isFullPath) {
					String url ="file://"+ Environment.getExternalStorageDirectory().getPath() + filePathString;
					super.loadUrl(url);
				} else {
//					filePathString ="file://"+ Environment.getExternalStorageDirectory().getPath() + "/nd/www/com.nd.commoncom.nd.h5module.hello/index2.html";
					super.loadUrl(filePathString);
				}
				appView.getView().setVisibility(View.GONE);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// 以后要设置成透明颜色，让html自身加背景，避免闪烁
		appView.getView().setBackgroundColor(Color.TRANSPARENT);
	}
	    
	@Override
	public Object onMessage(String id, Object data) {
		Log.e("star", "xxx:"+id);
		if (!TextUtils.isEmpty(id)) {
			if (id.equals("onPageFinished")) {
				animateIn(appView.getView());
			}
		}
		return super.onMessage(id, data);
	}
	
	private void animateIn(final View view) {
		
		view.setVisibility(View.VISIBLE);
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0);
		translateAnimation.setDuration(400);
		translateAnimation.setInterpolator(new AccelerateInterpolator());
		animationSet.addAnimation(translateAnimation);
		animationSet.setFillAfter(true);
		animationSet.setFillBefore(true); 
        view.startAnimation(animationSet);
        animationSet.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				view.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				appView.getView().setVisibility(View.VISIBLE);
			}
		});
    }
	
	long mLastTime = 0;
	public void animateOut() {
		long currentTime = System.currentTimeMillis();
		if (currentTime - mLastTime < 300) {
			return;
		}
		mLastTime = currentTime;
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				AnimationSet animationSet = new AnimationSet(true);
				TranslateAnimation translateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0);
				translateAnimation.setDuration(250);
				translateAnimation.setInterpolator(new DecelerateInterpolator());
				animationSet.addAnimation(translateAnimation);
				animationSet.setFillAfter(true);
				animationSet.setFillBefore(true); 
		        appView.getView().startAnimation(animationSet);
		        animationSet.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						finish();
					}
				});
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	public JSONObject getData() {
		return jsonObject;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			Toast.makeText(this, "onkeydown", 0).show();
			Intent intent = new Intent();
//			intent.putExtra(Plugin_intent.EXTRA_JSON, "resultdata a");
			setResult(RESULT_OK,intent);
			finish();
		}
		
		return super.onKeyDown(keyCode, event);
	}

}
