/**
 * Created by Joiningss on 12/12/14.
 */
module.exports = {
    //require
    ControlTitleKey: "title",//原生导航条标题
    ControlControllerName: "controllerName",//适于iOS的控制器名称
    ControlActivityName: "activityName", //适于于安卓的Activity名称
    ControlTargetPagePathKey: "targetPagePath",
    ControlTargetModuleIdKey: "targetModuleId",//模块ID
    ControlTransitionWithAnimKey: "transitionWithAnim",
    //require in model present transition
    ControlTransitionModelAnimTypeKey: "transitionModelAnimType",
    ControlTargetPageIDKey: "targetPageID",
    //optional, jump function will override the values for below keys
    ControlFromPageIDKey: "fromPageID",
    ControlTransitionTypeKey: "transitionType",
    // type enum
    ControlTransitionWithAnimType: {
        WITHOUT: 0,
        WITH: 1
    },
    ControlTransitionType: {
        NAV: 0,
        MODEL: 1
    },
    ControlTransitionModelAnimType: {
        CoverVertical: 0,
        FlipHorizontal: 1,
        CrossDissolve: 2,
        PartialCurl: 3
    }
};
