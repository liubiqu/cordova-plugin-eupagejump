var argscheck = require('cordova/argscheck'),
    exec = require('cordova/exec'),
    EUPageJumpConstants = require('./PageJump');

var pageJumpExport = {};
// Tack on the EUPageJumpConstants Constants to the base pageJump plugin.
for (var key in EUPageJumpConstants) {
    pageJumpExport[key] = EUPageJumpConstants[key];
}

/*
 * 导航控制器跳转方法 nav jump methods
 */
pageJumpExport.pushToPage = function (controlArgs, dataDic) {
    console.log('开始跳转到界面', controlArgs, dataDic);
    //TODO: check controlArgs
    var getValue = argscheck.getValue;
    var finalControlArgs = {},
        originalControlArgs = controlArgs || {};
    //require
    finalControlArgs[EUPageJumpConstants.ControlTitleKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTitleKey], "标题");
    finalControlArgs[EUPageJumpConstants.ControlActivityName] = getValue(originalControlArgs[EUPageJumpConstants.ControlActivityName], "");
    finalControlArgs[EUPageJumpConstants.ControlControllerName] = getValue(originalControlArgs[EUPageJumpConstants.ControlControllerName], "");
    var targetPagePath = convertRelateURLIfNeed(getValue(originalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey], "index.html"));
    finalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey] = targetPagePath;
    finalControlArgs[EUPageJumpConstants.ControlTargetModuleIdKey] = targetPagePath;
    //finalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey], "index.html");
    finalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey], EUPageJumpConstants.ControlTransitionWithAnimType.WITH);

    //optional, override by function
    //finalControlArgs[EUPageJumpConstants.ControlFromPageIDKey] =  getValue(originalControlArgs[EUPageJumpConstants.ControlFromPageIDKey], "unimplement");//关于pageID，需考证是否有可以直接引用的地方，目前采用nactive端来添加pageID
    finalControlArgs[EUPageJumpConstants.ControlTransitionTypeKey] = EUPageJumpConstants.ControlTransitionType.NAV;
    var onCallBack = function (callBackData) {
        console.log('跳转成功回调', callBackData);
        if (callBackData) {
            var isOnStart = parseInt(callBackData['onStart']);
            //alert('isOnStart:'+isOnStart);
            if (isOnStart == 0) {
                onClose(callBackData['data']);
            } else {
                onStart(callBackData['data']);
            }
        }
    };
    var onError = function (data) {
        console.log('跳转界面调用失败', data);
    };
    console.log(finalControlArgs, dataDic);
    exec(onCallBack, onError, "EUPageJump", "pushPage", [dataDic || {}, finalControlArgs]);
    console.log('跳转界面调用完成');
};

/*
 * nav jump methods
 */
pageJumpExport.pushPage = function (onStart, onClose, onError, dataDic, controlArgs) {
    //TODO: check controlArgs
    var getValue = argscheck.getValue;
    var finalControlArgs = {},
        originalControlArgs = controlArgs || {};
    //require
    finalControlArgs[EUPageJumpConstants.ControlTitleKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTitleKey], "标题");
    var targetPagePath = convertRelateURLIfNeed(getValue(originalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey], "index.html"));
    finalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey] = targetPagePath;
    finalControlArgs[EUPageJumpConstants.ControlTargetModuleIdKey] = targetPagePath;
    //finalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey], "index.html");
    finalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey], EUPageJumpConstants.ControlTransitionWithAnimType.WITH);

    //optional, override by function
    //finalControlArgs[EUPageJumpConstants.ControlFromPageIDKey] =  getValue(originalControlArgs[EUPageJumpConstants.ControlFromPageIDKey], "unimplement");//关于pageID，需考证是否有可以直接引用的地方，目前采用nactive端来添加pageID
    finalControlArgs[EUPageJumpConstants.ControlTransitionTypeKey] = EUPageJumpConstants.ControlTransitionType.NAV;
    var onCallBack = function (callBackData) {
        if (callBackData) {
            var isOnStart = parseInt(callBackData['onStart']);
            //alert('isOnStart:'+isOnStart);
            if (isOnStart == 0) {
                onClose(callBackData['data']);
            } else {
                onStart(callBackData['data']);
            }
        }
    };
    var onError = function (data) {
    };
    exec(onCallBack, onError, "EUPageJump", "pushPage", [dataDic || {}, finalControlArgs]);
};

pageJumpExport.popPage = function (onClose, onError, isRefreshParentPage) {
    exec(onClose, onError, "EUPageJump", "popPage", [isRefreshParentPage || false]);
};

pageJumpExport.popToRoot = function (onClose, onError, isRefreshParentPage) {
    exec(onClose, onError, "EUPageJump", "popToRoot", [isRefreshParentPage || false]);
};

/*
 * model jump methods
 */
pageJumpExport.presentModelPage = function (onStart, onClose, onError, dataDic, controlArgs) {
    //TODO: check controlArgs
    var getValue = argscheck.getValue;
    var finalControlArgs = {},
        originalControlArgs = controlArgs || {};
    //require
    finalControlArgs[EUPageJumpConstants.ControlTitleKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTitleKey], "untitled");
    var targetPagePath = convertRelateURLIfNeed(getValue(originalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey], "index.html"));
    finalControlArgs[EUPageJumpConstants.ControlTargetPagePathKey] = targetPagePath;
    finalControlArgs[EUPageJumpConstants.ControlTargetModuleIdKey] = targetPagePath;
    finalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTransitionWithAnimKey], EUPageJumpConstants.ControlTransitionWithAnimType.WITH);
    finalControlArgs[EUPageJumpConstants.ControlTransitionModelAnimTypeKey] = getValue(originalControlArgs[EUPageJumpConstants.ControlTransitionModelAnimTypeKey], EUPageJumpConstants.ControlTransitionModelAnimType.CoverVertical);
    //optional, override by function
    //finalControlArgs[EUPageJumpConstants.ControlFromPageIDKey] =  getValue(originalControlArgs[EUPageJumpConstants.ControlFromPageIDKey], "unimplement");//关于pageID，需考证是否有可以直接引用的地方,目前采用nactive端来添加pageID
    finalControlArgs[EUPageJumpConstants.ControlTransitionTypeKey] = EUPageJumpConstants.ControlTransitionType.MODEL;

    var onCallBack = function (callBackData) {
        if (callBackData) {
            var isOnStart = parseInt(callBackData['onStart']);
            if (isOnStart == 0) {
                onClose(callBackData['data']);
            } else {
                onStart(callBackData['data']);
            }
        }
    };
    exec(onCallBack, onError, "EUPageJump", "presentModelPage", [dataDic || {}, finalControlArgs]);
};

pageJumpExport.dismissModelPage = function (onClose, onError, dataDic) {
    exec(onClose, onError, "EUPageJump", "dismissModelPage", [dataDic || {}]);
};

/*
 * model jump methods
 */
pageJumpExport.refreshPage = function (onError, dataDic) {
    exec(function () {
    }, onError, "EUPageJump", "refreshPage", [dataDic || {}]);
}

/*
 * util methods
 */
//如果目标页面是relativeURL的形式，则转换成absoluteURL
function convertRelateURLIfNeed(pagePath) {
    var absolutePagePath;
    var a = document.createElement('a');
    a.href = pagePath;
    absolutePagePath = a.cloneNode(false).href;
    return absolutePagePath;
};
pageJumpExport.getFromData = function (successCallBack, errorCallBack) {
    exec(successCallBack, errorCallBack, "EUPageJump", "getFromData", []);
};
pageJumpExport.getResumeData = function (successCallBack, errorCallBack) {
    exec(successCallBack, errorCallBack, "EUPageJump", "getResumeData", []);
};
//Get pageID from page instance witch hold this plugin instance
pageJumpExport.getCurrentPageID = function (successCallBack, errorCallBack) {
    exec(successCallBack, errorCallBack, "EUPageJump", "getCurrentPageID", []);
};
pageJumpExport.getCurrentPageIndex = function (successCallBack, errorCallBack) {
    exec(successCallBack, errorCallBack, "EUPageJump", "getCurrentPageIndex", []);
};
module.exports = pageJumpExport;